# Installation d'un Moodle Vanilla
## Informations systèmes
- **Hyperviseur :** Oracle VM VirtualBox
- **OS version :** Linux Debian 9.1 (2019-02-19)
- **Configuration :**
    - **RAM :** 2GB
    - **vCore :** 2 
    - **Data :** 25GB

## Services 
- [x] UFW
- [x] OpenSSL
- [x] HAProxy  
- [ ] Vernish 
- [x] Apache2
- [x] PHP 7.3
- [x] MariaDB
- [x] Moodle

## Package de bases
### Ajout de paquets non présent après une fresh install 

```
apt install curl git vim ca-certificates apt-transport-https 

```

## Ufw (Firewall)
Utilisation d'une configuration bloquant tous sauf : 
- HTTP (80, 8080)
- HTTPS (443)
- SSH (22)


## Apache2
### Installation

```
apt install apache2

```

### Configuration
```
/etc/apache2/ports.conf
Listen 8080

<IfModule ssl_module>
	Listen 443
</IfModule>

<IfModule mod_gnutls.c>
	Listen 443
</IfModule>

$ systemctl restart apache2
```
## Installation PHP7.3 (Debian Stretch)
### Ajout du repo

```
wget -q https://packages.sury.org/php/apt.gpg -O- | sudo apt-key add -
echo "deb https://packages.sury.org/php/ stretch main" | sudo tee /etc/apt/sources.list.d/php.list
apt update & apt upgrade
```

### Installation & dépendances pour Moodle

```
apt install php7.3 (install le module pour apache2 par défaut)
apt install php7.3-curl php7.3-gd php7.3-zip php7.3-soap php7.3-xml php7.3-mysql php7.3-intl php7.3-mbstring php7.3-xmlrpc 
systemctl restart apache2
```
## OpenSSL
### Création d'un certificat self-signed 

```
openssl req --newkey rsa:4096 -nodes -keyout key.pem -x509 -days 365 -out certif.pem 
```
### Création fichier `pem` si nécessaire
Dans le cas où les fichiers générés sont en `.crt` & `.key`, il faut un `.pem` pour haproxy.
Il suffit bêtement de concaténer le contenu des 2 fichiers en un seul : 
```bash
$ cat maKey.key monCert.crt > monCert.pem
```

J'ai triché, je l'ai fait avec letsEncrypt et certbot :(

## HAProxy
> Version "stable" depo Debian 9.1 : 1.7.5-2 2017/05/17
> Version "stable" depo Added : 1.9.4-1 2019/02/07

### Installation dernière version stable
Pour installer la version `1.8-stable (LTS)` de HAProxy : 

```
$ echo deb http://httpredir.debian.org/debian stretch-backports main | tee /etc/apt/sources.list.d/backports.list
$ apt update
$ apt-get install haproxy=1.8.\* -t stretch-backports
```

### Configuration du fichier de conf 
```

$ /etc/haproxy/haproxy.cfg

    global
    	log 127.0.0.1 local2
    	chroot /var/lib/haproxy 
    	user haproxy
    	group haproxy 
    	daemon
    	maxconn 2048
    	tune.ssl.default-dh-param 2048
    	ca-base /path/to/certs/

    defaults
    	log global 
    	mode http
    	option httplog
    	option dontlognull
    	option forwardfor
    	option http-server-close
    	timeout connect 20s
     	timeout client 20s
    	timeout server 20s

    frontend www-http
    	bind *:80
    	reqadd X-Forwarded-Proto:\ http
    	default_backend moodle_web

    frontend www-https
    	bind *:443 ssl crt /path/to/certs/cert.pem
    	reqadd X-Forwarded-Proto:\ https
    	option httplog
            default_backend moodle_web

    backend moodle_web 
    	redirect scheme https if !{ ssl_fc }
            cookie SERVERID insert indirect nocache
            # port de Varnish
            server web1 127.0.0.1:8081
```

## MariaDB
### Installation MariaDB 

```
apt install mariadb-server
```

Exécution du fichier de configuration de sécurité pour BDD 

```
mysql_secure_installation
# Edit root password
# Disable anonymous users
# Disable root login remote
# Reload new params
```

- Création base de données pour Moodle & affection utilisateur
```
# Spécifie l'encodage en UTF-8 pour éviter des problèmes
CREATE DATABASE moodle DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
# Création utilisateur ayant les accès uniquement sur la base de données Moodle
GRANT SELECT,INSERT,UPDATE,DELETE,CREATE,CREATE TEMPORARY TABLES,DROP,INDEX,ALTER ON moodle.* TO moodleDBA@localhost IDENTIFIED BY '!@password';
```
L'utilisateur moodleDBA a le full right sur la db moodle; mais pour raison de sécurité, on peut limiter certains droits (DROP & DELETE, par exemple).

- Compresser les données et mettre au format Barracuda 

```mysql
use moodle;
SET GLOBAL innodb_file_per_table= 1;
SET GLOBAL innodb_large_prefix= on;
SET GLOBAL innodb_file_format= "Barracuda";
```
- Pour affecter les variables après reboot du service/système, il faut ajoute lignes dans un fichier `custom` dans `/etc/mysql/mariadb.conf.d/`

```mysql
$ /etc/mysql/mariadb.conf.d/my.cnf
innodb_file_per_table= 1;
innodb_large_prefix= on;
innodb_file_format= "Barracuda";
```

## Moodle
### Installation via master branch (git)
- Cloner le dernier repo (stable) sur le git de Moodle à l'intérieur du webroot directory
```
$ cd /var/www/
$ git clone -b MOODLE_36_STABLE git://git.moodle.org/moodle.git
```
- Il faut que le dossier soit accessible par apache2 mais on ne peut pas écire dedans, on donne donc les droits à `www-data`
```bash
$ chown -R root:www-data /var/www/moodle
$ chmod 750 /var/www/moodle
```
- Création du dossier des données (stockage) de Moodle. (Eviter le dossier webroot)

```bash
$ mkdir -p /srv/data/moodle
# on peut créer un groupe spéciale pour écrire dans le dossier
chown -R doodle:doodle /srv/data/moodle
chmod 770 /srv/data/moodle
```

### Création d'un vHost sous Apache2

```
$ cp /etc/apache2/sites-availables/000-default.conf ./moodle.conf

<VirtualHost *:8080>
	ServerAdmin cgx.contact@gmail.com 
	DocumentRoot /var/www/moodle
	
	ErrorLog ${APACHE_LOG_DIR}/error.log
	CustomLog ${APACHE_LOG_DIR}/access.log combined
</VirtualHost>


```

### Initiation de Moodle

Une fois le dossier complèter cloner, on peut exécuter la première configuration de 3 manières différentes :
1. Première exécution sur le serveur Web (HTML)
2. Configuration via admin/cli qui crée un fichier `config.php` à la source de `./moodle` 

```
$ cd /var/www/moodle/admin/cli
$ /usr/bin/php install.php
```
3. Configuration directe du `config.php` en se basant sur le template `config-dist.php`

```
$ cp /var/www/moodle/config-dist.php ./config.php
```

## Varnish 
Installation de Varnish via dépot officiel de Debian version stable `5.0-lts`
`$ apt install varnish`

Configuration du fichier systemd pour modification du "watch port" et du port sur lequel celui-ci tourne :
```
$ systemctl stop varnish
$ cp /lib/systemd/system/varnish.service /etc/systemd/system/ && vim varnish.service
```

```system
# on edit la ligne d'exec
ExecStart=/usr/sbin/varnishd -j unix,user=vcache -F -a :8081 -T localhost:6082 -f /etc/varnish/default.vcl \
-S /etc/varnish/secret -s malloc,256m

```
- `-a` : indique sur quel port est le service (on peut mettre 80 par défault, mais ici haproxy est sur le port 80 et apache2 sur le 8080.
- `-f` : fichier de config utiliser 
- `-s malloc,256m` : taille de stockage en ram utilisé par varnish, selon les besoins. (m/G)

### Mise en place des règles 
- Modification du fichier par défaut : `/etc/varnish/default.vcl`

```vcl
# information backend (apache2)
backend moodle {
  .host = "127.0.0.1";
  .port = "8080";
  .first_byte_timeout = 180s;
  }
  
# vcl_recv 
# règles indiquant si le contenu a besoin d'être en cache où si les requêtes sont directement envoyées vers le backend
sub vcl_recv {  
  if (req.url ~ "/(theme|lib)") {
    return (hash);
  }
  if (req.url ~ "\.(css|js|html)$") {
    return (hash);
  }
  return(hash);
}
  
# vcl_backend_resp
sub vcl_backend_response {
  if (beresp.ttl <= 0s || beresp.http.Set-Cookie || beresp.http.Vary == "*") {
    if(bereq.url ~ "/(theme|lib)") {
      set beresp.http.cache-control = "public, max-age=259200";
      set beresp.ttl = 3d;
    } elseif (bereq.url ~ "\.(css|js|html)$") {
      set beresp.http.cache-control = "public, max-age=259200";
      set beresp.ttl = 3d;
    }
  }
  return (deliver);
}
```

### Test de la configuration
1. Vérification via `curl -I adress` qui renvoie le HEADER HTML
2. Vérification de la mise en cache via la console du navigateur Internet
3. Vérification via https://isvarnishworking.uk/ (si site en ligne)
4. Vérification des logs d'Apache2 et Varnish + Comparaison.










