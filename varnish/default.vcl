# version 4
# https://github.com/mattiasgeniar/varnish-4.0-configuration-templates/blob/master/default.vcl
vcl 4.0;

backend moodle {
  .host = "127.0.0.1";
  .port = "8080";
  .first_byte_timeout = 180s;
 # probe check the health of the backend
#   .probe = {
#   .request =
#     "HEAD /HTTP/1.1"
#     "Host: localhost"
#     "Connection: close"
#     "User-Agent: Varnish Health Probe";
#   .timeout = 1s; # timeout after 10s
#   .interval = 10s; # check for backend, every 10m
#   .window = 5;   # "number of try/test"
#   .threshold =3; # if 3/5 succeed, backend is healthy
# }
}

sub vcl_recv {
  # check if cache is needed
  # only deals with GET & HEAD
  if (req.method != "GET" && req.method != "HEAD") {
    return (pipe);
  } 
  if (req.url ~ "/(theme|lib)") {
    return (hash);
  }
  if (req.url ~ "\.(css|js|html)$") {
    return (hash);
  }
#  if (req.http.Authorization || req.http.Cookie ~ "(^|; )(__ac=|_ZopeId=)") {
#    return (pipe);
#  }
  return (hash);
}

sub vcl_backend_response {
  if (beresp.ttl <= 0s || beresp.http.Set-Cookie || beresp.http.Vary == "*") {
    if(bereq.url ~ "/(theme|lib)") {
      set beresp.http.cache-control = "public, max-age=259200";
      set beresp.ttl = 3d;
    } elseif (bereq.url ~ "\.(css|js|html)$") {
      set beresp.http.cache-control = "public, max-age=259200";
      set beresp.ttl = 3d;
    }
  }
  return (deliver);
}


